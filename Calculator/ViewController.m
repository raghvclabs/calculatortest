//
//  ViewController.m
//  Calculator
//
//  Created by Clicklabs 104 on 9/10/15.
//  Copyright (c) 2015 pooja. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

UILabel *enter;
UIButton *AC;
UIButton *sign;
UIButton *percentage;
UIButton *divide;
UIButton *multiply;
UIButton *plus;
UIButton *minus;
UIButton *point;
UIButton *equalsto;
UIButton *one;
UIButton *two;
UIButton *three;
UIButton *four;
UIButton *five;
UIButton *six;
UIButton *seven;
UIButton *eight;
UIButton *nine;
UIButton *zero;
UIImageView *imgview;
int Plus=0;

- (void)viewDidLoad {
    [super viewDidLoad];
    //backgroung image
   UIImageView *imgview1 = [UIImageView new];
    imgview1 .frame = CGRectMake(7, 9, 305, 550);
    imgview1 .image = [UIImage imageNamed:@"black.jpg"];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"black.png"]];

    //label
    enter=[[UILabel alloc]init];
    enter.textColor=[UIColor whiteColor];
   //enter.backgroundColor=[UIColor darkGrayColor];
    enter.frame=CGRectMake(0,0,400,149);
    [enter setFont:[UIFont boldSystemFontOfSize:50]];
    [self.view addSubview:enter];
    //AC button
    AC=[[UIButton alloc]init];
    AC.frame=CGRectMake(0,150,80,80);
    AC.backgroundColor=[UIColor lightGrayColor];
    [AC setTitle:@"AC" forState:UIControlStateNormal];
    [AC setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:AC];
    //+/-
    sign=[[UIButton alloc]init];
    sign.frame=CGRectMake(81,150,80,80);
    sign.backgroundColor=[UIColor lightGrayColor];
    [sign setTitle:@"+/-" forState:UIControlStateNormal];
    [sign setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:sign];
    //%
    percentage=[[UIButton alloc]init];
    percentage.frame=CGRectMake(162,150,80,80);
    percentage.backgroundColor=[UIColor lightGrayColor];
    [percentage setTitle:@"%" forState:UIControlStateNormal];
    [percentage setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:percentage];
    //divide
    divide=[[UIButton alloc]init];
    divide.frame=CGRectMake(243,150,80,80);
    divide.backgroundColor=[UIColor orangeColor];
    [divide setTitle:@"/" forState:UIControlStateNormal];
    [divide setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:divide];
    //7
    seven=[[UIButton alloc]init];
    seven.frame=CGRectMake(0,231,80,80);
    seven.backgroundColor=[UIColor lightGrayColor];
    [seven setTitle:@"7" forState:UIControlStateNormal];
    [seven setFont:[UIFont boldSystemFontOfSize:30]];
    [seven addTarget:self action:@selector(sevenpressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:seven];
    //8
    eight=[[UIButton alloc]init];
    eight.frame=CGRectMake(81,231,80,80);
    eight.backgroundColor=[UIColor lightGrayColor];
    [eight setTitle:@"8" forState:UIControlStateNormal];
    [eight setFont:[UIFont boldSystemFontOfSize:30]];
    [eight addTarget:self action:@selector(eightpressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:eight];
    //9
    nine=[[UIButton alloc]init];
    nine.frame=CGRectMake(162,231,80,80);
    nine.backgroundColor=[UIColor lightGrayColor];
    [nine setTitle:@"9" forState:UIControlStateNormal];
    [nine setFont:[UIFont boldSystemFontOfSize:30]];
    [nine addTarget:self action:@selector(ninepressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:nine];
    //*
    multiply=[[UIButton alloc]init];
    multiply.frame=CGRectMake(243,231,80,80);
    multiply.backgroundColor=[UIColor orangeColor];
    [multiply setFont:[UIFont boldSystemFontOfSize:30]];
    [multiply setTitle:@"X" forState:UIControlStateNormal];
    [self.view addSubview:multiply];
    //4
    four=[[UIButton alloc]init];
    four.frame=CGRectMake(0,312,80,80);
    four.backgroundColor=[UIColor lightGrayColor];
    [four setTitle:@"4" forState:UIControlStateNormal];
    [four setFont:[UIFont boldSystemFontOfSize:30]];
    [four addTarget:self action:@selector(fourpressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:four];
    //5
    five=[[UIButton alloc]init];
    five.frame=CGRectMake(81,312,80,80);
    five.backgroundColor=[UIColor lightGrayColor];
    [five setTitle:@"5" forState:UIControlStateNormal];
    [five setFont:[UIFont boldSystemFontOfSize:30]];
    [five addTarget:self action:@selector(fivepressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:five];
    //6
    six=[[UIButton alloc]init];
    six.frame=CGRectMake(162,312,80,80);
   six.backgroundColor=[UIColor lightGrayColor];
    [six setTitle:@"6" forState:UIControlStateNormal];
    [six setFont:[UIFont boldSystemFontOfSize:30]];
    [six addTarget:self action:@selector(sixpressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:six];
    //-
    minus=[[UIButton alloc]init];
    minus.frame=CGRectMake(243,312,80,80);
    minus.backgroundColor=[UIColor orangeColor];
    [minus setTitle:@"-" forState:UIControlStateNormal];
    [minus setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:minus];
    //1
    one=[[UIButton alloc]init];
    one.frame=CGRectMake(0,393,80,80);
    one.backgroundColor=[UIColor lightGrayColor];
    [one setTitle:@"1" forState:UIControlStateNormal];
    [one setFont:[UIFont boldSystemFontOfSize:30]];
    [one addTarget:self action:@selector(onepressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:one];
    //2
    two=[[UIButton alloc]init];
    two.frame=CGRectMake(81,393,80,80);
    two.backgroundColor=[UIColor lightGrayColor];
    [two setTitle:@"2" forState:UIControlStateNormal];
    [two setFont:[UIFont boldSystemFontOfSize:30]];
    [two addTarget:self action:@selector(twopressed:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.view addSubview:two];
    //3
    three=[[UIButton alloc]init];
    three.frame=CGRectMake(162,393,80,80);
    three.backgroundColor=[UIColor lightGrayColor];
    [three setTitle:@"3" forState:UIControlStateNormal];
    [three setFont:[UIFont boldSystemFontOfSize:30]];
    [three addTarget:self action:@selector(threepressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:three];
    //+
    plus=[[UIButton alloc]init];
    plus.frame=CGRectMake(243,393,80,80);
    plus.backgroundColor=[UIColor orangeColor];
    [plus setTitle:@"+" forState:UIControlStateNormal];
    [plus setFont:[UIFont boldSystemFontOfSize:30]];
    [plus addTarget:self action:@selector(plusbutton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:plus];
    //0
    zero=[[UIButton alloc]init];
    zero.frame=CGRectMake(0,474,161,95);
    zero.backgroundColor=[UIColor lightGrayColor];
    [zero setTitle:@"0" forState:UIControlStateNormal];
    [zero setFont:[UIFont boldSystemFontOfSize:30]];
    [zero addTarget:self action:@selector(zeropressed:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:zero];
    //point
    point=[[UIButton alloc]init];
    point.frame=CGRectMake(162,474,80,95);
    point.backgroundColor=[UIColor lightGrayColor];
    [point setTitle:@"." forState:UIControlStateNormal];
    [point setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:point];
    //=
    equalsto=[[UIButton alloc]init];
    equalsto.frame=CGRectMake(243,474,90,95);
    equalsto.backgroundColor=[UIColor orangeColor];
    [equalsto setTitle:@"=" forState:UIControlStateNormal];
    [equalsto setFont:[UIFont boldSystemFontOfSize:30]];
    [self.view addSubview:equalsto];
    // Do any additional setup after loading the view, typically from a nib.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
